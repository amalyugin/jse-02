package ru.t1.malyugin.tm;

import ru.t1.malyugin.tm.constant.ArgumentConst;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
    }

    public static void processArguments(final String[] args) {
        if (args == null || args.length < 1) {
            showError();
            return;
        }
        processArgument(args[0]);
    }

    public static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showError();
                break;
        }
    }

    public static void showVersion() {
        System.out.printf("[VERSION]\n%s\n", "1.2.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexej Malyugin");
        System.out.println("E-mail: amalyugin@t1-consulting.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show version info.\n", ArgumentConst.VERSION);
        System.out.printf("%s - Show Author info.\n", ArgumentConst.ABOUT);
        System.out.printf("%s - Show command list.\n", ArgumentConst.HELP);
    }

    public static void showError() {
        System.err.println("[ERROR]\nCurrent argument is not available");
    }

}